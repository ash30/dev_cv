## Curriculum Vitae
**QA Engineer with 3 years commerical engineering experience and strong technical understanding, able to safeguard stability and maturely manage risk to reliably release great products.**

I advocate effecient and maintainable automated intergration tests within a continous delivery build process 
and believe when coupled with careful change management, data driven regression hunting and user centered critical path testing I can bring stability to any fast moving agile development.

In my personal time I like to blog about my personal dev projects, study mandarin and publish short educational animations.

### Skills 
#### Testing
* Able to develop and execute efficient functionality, regression and acceptance test plans.
* Ability to automate tests and pragmatically work around platform idiosyncrasies.
* Comfortable working across OS X, iOS, Windows and Linux releases.
* Knowledge of bug management tools and Agile methodologies.
* Good technical knowledge and comfortable discussing issues with developers.
* Communicate effectively with other disciplines to help resolve issues.

#### Python
* Knowledge of OOP test frameworks like PyUnit
* Ability to develop maintainable automated regression tests suits using image comparison and output matching.
* Able to write command-line tools quickly using standard library and argparse.
* Always looking to evolve QA process and automate recurring work.
* Experience of jython during testing sikuli for automated GUI testing

#### Other Skills 
Additionally I would be happy to discuss my experience of any of the following skills: JS, HTML, CSS, Sass, 3D Animation, Bash, GIT, Perforce, Jenkins, objective C, Jira, Target Process, Bugzilla, Selenium WebDriver, Vmware, Vagrant 

### Open source and writing 

* #### TPAPI
  A python library for interacting with project management service Target Process<br>
  <a href="http://www.github.com/ash30/tpapi">www.github.com/ash30/tpapi</a>

* #### Personal Blog 
  Various articles about personal projects, python and Testing <br>
  <a href="http://www.ashleyarthur.co.uk">www.ashleyarthur.co.uk</a>

### Experience 
#### QA Engineer 
The Foundry; May 2013 - Currently <br>
<span class=skills_list> Skills: Testing, Agile, Python, Perforce, Bash, Continuous development </span>

Hired to ensure quality in porting of 3D content application to linux and then to verify new networking feature development.
I later moved to internal startup to ensure quality and useability of early prototypes and took ownership and drove technical image quality through automated testing.

Key Achievements: 

* Planned, developed and executed test plans for new features.
* Worked independently and proactively managed tasks whilst working remotely from dev team.
* Tested backend REST api services for trial licensing and application updates.
* Automated testing for prototype application, writing maintainable tests in the face of changing, undocumented api's.
* Helped run iOS beta program using test flight to widen hardware coverage and capture user issues.


#### Creature Effects TD 
Moving Picture Company; Sept 2011 - May 2013 <br>
<span class=skills_list> Skills: 3D animation, Python, Linux, Bash, GIT, Technical Animation </span>

Animated Character technical director creating content for hollywood movies using physical simulators of cloth and hair.

Key Achievements:

* Interpreting client requirements 
* Delivering content on time in a fast paced environment
* Large data set automation through python scripting

#### Data Assistant (Internship)
Utrack; June 2010 - Sept 2010

Data Assistant and remote trainer for geographical web service 

### Education
BA Computer Animation (2.1): University of Hertfordshire
